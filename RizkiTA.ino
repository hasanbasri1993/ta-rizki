#include <Servo.h>
#include <LedFlasher.h>
#include <Wire.h>
#include <LiquidCrystal_I2C.h>
LiquidCrystal_I2C lcd(0x27, 2, 1, 0, 4, 5, 6, 7, 3, POSITIVE);  // Set the LCD I2C address

int pinbuzzer = 12;
int pinledgasabis = 11;
int pinservopemantik = 8;
int pinservogas = 9;
int pinsensorapi = 10;
int pinsensorair = A1;
int kapasitasair = 500;

int kalimantik = 0;
unsigned long previousMillis = 0;

//pin pin komunikasi dengan NODEMCU
int pinpemicupemantik    = 19;
int pinkeadaanapi        = 20;
int pinpemicupembakaran1 = 21;
int pinpemicupembakaran2 = 22;
int pinpemicupembakaran3 = 23;
///////////////////////////////////


//Silahkan di ubah dengan sesuai selera
const long percobaan1 = 600000; //10 menit
const long percobaan2 = 1200000; //20 menit
const long percobaan3 = 1800000; //30 menit

Servo servogas, servopemantik;
LedFlasher ledgasabismenyala (pinledgasabis, 200, 300);
LedFlasher ledgasabismati (pinledgasabis, 0, 0);

void setup()
{
  Serial.begin(9600);  // Used to type in characters
  ledgasabismenyala.begin(); //setup led stobo buat peringatan gas abis, menyala
  ledgasabismati.begin();//setup led stobo buat peringatan gas abis, mati
  servogas.attach(pinservogas); //Pin servo gas
  servopemantik.attach(pinservopemantik); //Pin servo pemantik
  pinMode(pinpemicupemantik, INPUT); //pin pemicu adalah Input Mode, apakah ada signal dari nodemcu
  pinMode(pinpemicupembakaran1, OUTPUT); //pin pemicu adalah Input Mode, apakah ada signal dari nodemcu
  pinMode(pinpemicupembakaran2, OUTPUT); //pin pemicu adalah Input Mode, apakah ada signal dari nodemcu
  pinMode(pinpemicupembakaran3, OUTPUT); //pin pemicu adalah Input Mode, apakah ada signal dari nodemcu
  pinMode(pinkeadaanapi, OUTPUT); //pin pemicu adalah Input Mode, apakah ada signal dari nodemcu
  digitalWrite(pinpemicupembakaran1, LOW) //set low / mati bisa pertama di nyalakan,
  digitalWrite(pinpemicupembakaran2, LOW) //set low / mati bisa pertama di nyalakan,
  digitalWrite(pinpemicupembakaran3, LOW) //set low / mati bisa pertama di nyalakan,
  digitalWrite(pinkeadaanapi, LOW) //set low / mati bisa pertama di nyalakan,
  lcd.begin(16, 2);  // initialize the lcd for 16 chars 2 lines, turn on backlight
  lcd.backlight(); // finish with backlight on
  lcd.setCursor(0, 0); //Start at character 4 on line 0
  lcd.print("Hello, world!");
  delay(1000);
  lcd.setCursor(0, 1);
  lcd.print("HI!YourDuino.com");
}


void loop()
{
  // Jika ada signal dari NodeMCU bahwa  tombol pemicu ditekan
  if (digitalRead(pinpemicupemantik) == HIGH ) {
    pembakaran("Pembakaran 1", ""); //manggil fungsi pembakaran awal
    digitalWrite(pinpemicupembakaran1, HIGH); // beri sinyal high / nyala ke NODEMCU pembakaran 1 telah dimulai
    delay(percobaan1);
    digitalWrite(pinpemicupembakaran1, LOW); // beri sinyal low / mati ke NODEMCU pembakaran 1 telah selesai
    if (analogRead(pinsensorair) <= kapasitasair) {
      pembakaran("Pembakaran 2", ""); //manggil fungsi pembakaran kedua
      digitalWrite(pinpemicupembakaran2, HIGH); // beri sinyal high / nyala ke NODEMCU pembakaran 2 telah dimulai
      delay(percobaan2);
      digitalWrite(pinpemicupembakaran2, LOW); // beri sinyal low / mati ke NODEMCU pembakaran 2 telah selesai
      if (analogRead(pinsensorair) <= kapasitasair) {
        pembakaran("Pembakaran 3", ""); //manggil fungsi pembakaran ketiga
        digitalWrite(pinpemicupembakaran3, HIGH); // beri sinyal high / nyala ke NODEMCU pembakaran 3 telah dimulai
        delay(percobaan3);
        digitalWrite(pinpemicupembakaran3, LOW); // beri sinyal low / mati ke NODEMCU pembakaran 3 telah selesai
      }
    }
  }
}

void pembakaran(String baris1, String baris2) {
  //maka servo gas berputar ke 45 dan servo pemnatik ke 180
  servogas.write(45); //servo gas berputar ke 45
  servopemantik.write(180);//servo pemantik berputar ke 180
  servopemantik.write(0);//servo pemantik kembali ke 0
  while (digitalRead(pinsensorapi) == LOW)
  {
    kalimantik++;
    servogas.write(45); //servo gas berputar ke 45
    servopemantik.write(180);//servo pemantik berputar ke 180
    if (kalimantik >= 3 ) {
      break;
      info("Gas Habis !!!", "isi ulang gas");
      ledgasabismenyala.update();
    }
  }
  
  digitalWrite(pinkeadaanapi, HIGH); //beri sinyal node api menyala
  tone(pinbuzzer, 200, 600);
  tone(pinbuzzer, 200, 600);
  tone(pinbuzzer, 200, 600);

  info(baris1, baris2);
  ledgasabismati.update();

}

void info(String baris1, String baris2) {

  //lcd menginformasikan bahwa servo telah
  lcd.clear();
  lcd.setCursor(0, 0); //Start at character 4 on line 0
  lcd.print(baris1);
  delay(1000);
  lcd.setCursor(0, 1);
  lcd.print(baris2);
}

